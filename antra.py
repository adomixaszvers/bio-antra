import re
import sys
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np
from Bio.Blast import NCBIWWW, NCBIXML
from shutil import copyfileobj
import os.path
from tabulate import tabulate
from Bio import Entrez
import random


USE_OLD_DATA = False
USE_PLOT = False


def search_for_best_hit(records):
    sequences = ''.join([record.format('fasta') for record in records])
    with open('records.fasta', 'w') as handle:
        handle.write(sequences)
    ncbi = NCBIWWW.qblast(program='blastn', database='nt', sequence=sequences, entrez_query='bacteria[Organism]')
    return ncbi


def iround(number):
    if number < 0:
        return int(number - 0.5)
    else:
        return int(number + 0.5)


def cg_percent(seq):
    return iround((seq.count("C") + seq.count("G"))*100/len(seq))


def main(filename):
    """
    https://en.wikipedia.org/wiki/FASTQ_format#Encoding
    """
    pattern = re.compile(r'@(?P<seq_id>.+)\n(?P<seq>.+)\n\+\n(?P<qual>.+)')
    with open(filename, "r") as handle:
        reads_string = handle.read()

    matches_iter = re.finditer(pattern, reads_string)

    min_value, max_value = None, None

    for match in matches_iter:
        quality_string = match.group('qual')
        iter_min = min(quality_string)
        iter_max = max(quality_string)
        if not min_value:
            min_value = iter_min
        elif min_value > iter_min:
            min_value = iter_min

        if not max_value:
            max_value = iter_max
        elif max_value < iter_max:
            max_value = iter_max

    formats = []
    if min_value >= '!' and max_value <= 'I':
        formats.append('Sanger Phred+33')
    if min_value >= '!' and max_value <= 'J':
        formats.append('Illumina 1.8+ Phred+33')
    if min_value >= ';' and max_value <= 'h':
        formats.append('Solexa Solexa+64')
    if min_value >= '@' and max_value <= 'h':
        formats.append('Illumina 1.3+ Phred+64')
    if min_value >= 'B' and max_value <= 'h':
        formats.append('Illumina 1.5+ Phred+64')


    print('a)')
    for fmt in formats:
        print(fmt)

    pattern = re.compile(r'(?P<instrument>\w+):(?P<run_number>\d+):(?P<flowcell_ID>[a-zA-Z0-9_\-]+):(?P<lane>\d+):(?P<tile>\d+):(?P<x_pos>\d+):(?P<y_pos>\d+) (?P<read>\d+):(?P<is_filtered>Y|N):(?P<control_number>\d+):(?P<index_sequence>\w+)')

    with open(sys.argv[1], 'r') as handle:
        i = 0
        data = dict()
        reads = SeqIO.parse(handle, 'fastq')
        for record in reads:
            key = cg_percent(record.seq)
            if not key in data:
                data[key] = []
            data[key].append(record)

    if USE_PLOT:
        x = []
        y = []

        for k in sorted(data.keys()):
            x.append(k)
            y.append(len(data[k]))

        plt.bar(x, y)
        plt.title('b)')
        plt.xlabel('procentai')
        plt.ylabel('read\'u skaičius')
        plt.show()

    #nustatomi iš diagramos
    peaks = [34, 53, 70]

    records = []
    for peak in peaks:
        records = records + random.sample(data[peak], 5)
    found_hits = []

    if not os.path.exists('blast.xml') or not USE_OLD_DATA:
        with open('blast.xml', 'w') as handle:
            ncbi = search_for_best_hit(records)
            ncbi.seek(0)
            copyfileobj(ncbi, handle)

    table = []
    headers = ['read\'o id', 'mikroorganizmo rūšis']
    with open('blast.xml', 'r') as handle:
        blast = NCBIXML.parse(handle)
        titles = [record.alignments[0].title.split('|')[4] for record in blast]
        for i in range(len(records)):
            table.append([records[i].description, titles[i]])
    print('c)')
    print(tabulate(table, headers=headers, tablefmt="fancy_grid"))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit()
    main(sys.argv[1])